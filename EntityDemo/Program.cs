﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityDemo
{
   partial class Employee
    {
        public string FullName { get => $"{FirstName} {LastName}"; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            using (NorthwindEntities nw = new NorthwindEntities())
            {
                var categories = nw.Categories;
                var products = nw.Products;
                categories.Add(new Category() {
                                CategoryName = "Krokodillid",
                                Description = "Allgaatorid, Krokodillid ja muud suured sisalikku" });
                var x = nw.SaveChanges();
            }
        }
    }
}
